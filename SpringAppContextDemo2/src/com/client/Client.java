package com.client;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.model.Employee;

public class Client {

	public static void main(String[] args) {

		ApplicationContext context = null;
		
		try {
	//		context = new FileSystemXmlApplicationContext("C:\\Users\\ebergin\\eclipse-workspace\\SpringAppContextDemo2\\src\\applicationContext.xml");
			context = new FileSystemXmlApplicationContext("config/applicationContext.xml");
			
			Employee emp = context.getBean("employee", Employee.class);
			System.out.println(emp.getEmployeeId() + "\t" + emp.getName() );
		} catch (BeansException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if(context != null)
			{
				((AbstractApplicationContext) context ).close();
			}
		}
		
		
	}

}
